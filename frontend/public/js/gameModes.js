const gameMode = 0;

function addPointToPlayer(numberTouched, circleTouched, posX, posY) {

  if (gameMode == 0) {
    const score = circleToScore(numberTouched, circleTouched);
    if (gameMode == 0) {
      roomManager.addPoint(score, circleTouched, posX, posY);
    }
    else if (gameMode == 1) {
      if (score == roomManager.playerPlaying.score + 1 || score == 100) {
        roomManager.addPoint(1, circleTouched, posX, posY);
      }
      else {
        roomManager.addPoint(0, circleTouched, posX, posY);
      }
    }
  }

  else if (gameMode == 1) {
    let score;
    if (circleTouched == 0 ||circleTouched == 1) {
      score = roomManager.playerPlaying.score + 1;
    }
    else {
      score = numberTouched;
    }

    if (score == roomManager.playerPlaying.score + 1 || score == 100) {
      roomManager.addPoint(1, circleTouched, posX, posY);
    }
    else {
      roomManager.addPoint(0, circleTouched, posX, posY);
    }
  }
}