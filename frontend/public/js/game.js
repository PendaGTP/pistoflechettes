$(document).ready(function(){
  $('.modal').modal();

  // Automatic hide turns when click somewhere
  $('body').click(function(e) {
    if(e.target.id == "showTurns_btn" || $(e.target).parents('#showTurns_btn').length > 0 || e.target.id == "hideTurns_btn" || $(e.target).parents('#hideTurns_btn').length > 0 || $("#turnsContainer").css("display") == "none" || e.target.id == "turnsContainer" || $(e.target).parents('#turnsContainer').length > 0 ) {
      return false;
    }
    showHideTurns();
  });
});


class RoomManager {
  constructor(id, arrowsPerTurn, limitPoints, limitRounds) {
    this.players = [];
    this.id = id;
    this.arrowsPerTurn = arrowsPerTurn;
    this.limitPoints = limitPoints;
    this.limitRounds = limitRounds;
    this.indexPlayerPlaying = 0;
    this.playerPlaying = null;
    this.arrowsLeft = this.arrowsPerTurn;
  
    this.actualRound = 0;
    this.actualTurn = 0;
    this.actualDart = 0;
  
    this.roomStatus = {};
  
    this.setupHTMLPage();
  }
  setupHTMLPage() {
    $("#leftArrowsPart").css("height", (this.arrowsPerTurn*8+(0.2*this.arrowsPerTurn))+"vh");
    $("#leftArrowsPart").css("top", ((50 - this.arrowsPerTurn/2*7)) + "vh");
  
    if (gameAlreadyStarted == 2) {
      $("#pauseRoom_btn").css("display", "none");
      document.getElementById("endRoom_btn").onclick = () => {location.href = "index.html";}
    }
  }

  addPoint(score, circleTouched, posX, posY) {
    this.postDart(score, circleTouched, posX, posY);
  }

  scoreToHTML(score) {
    this.playerPlaying.score += score;

    $(`#${this.playerPlaying.id} > .playerPart > .scorePlayer`).text(this.playerPlaying.score);
    $("#playingArrows .arrow:nth-child("+(this.arrowsPerTurn-this.arrowsLeft+1)+")").text(score);
    this.arrowsLeft--;
    this.updateArrowsLeftHTML();
    if (this.arrowsLeft == 0) {
      if (this.indexPlayerPlaying+1 >= this.players.length) {
        this.createRound();
      }
      else {
        this.indexPlayerPlaying++;
        this.changePlayerPlaying();
      }
    }
  }

  postDart(score, circleTouched, posX, posY) {
    this.actualDart++;
  
    if (gameAlreadyStarted != 0) {
      this.scoreToHTML(score);
      return;
    }

    let data ={
      "dart_number": this.actualDart,
      "label": score,
      "score": score,
      "position": {
        "x": posX, 
        "y": posY,
        "depth": circleTouched.level
      }
    }
    requestManager.post("rooms/"+this.id+"/turns/"+this.actualTurn+"/players/"+this.playerPlaying.id+"/darts", data, () => {this.scoreToHTML(score);});
  }

  changePlayerPlaying() {
    if (this.playerPlaying != null) {
      $("#"+this.playerPlaying.id).removeClass("actualPlayer");
      $("#"+this.playerPlaying.id).addClass("notActualPlayer");
    }

    this.playerPlaying = this.players[this.indexPlayerPlaying];
    
    this.arrowsLeft = this.arrowsPerTurn;

    $("#"+this.playerPlaying.id).addClass("actualPlayer");
    $("#"+this.playerPlaying.id).removeClass("notActualPlayer");
  
    this.updateArrowsLeftHTML();

    this.createTurn();
  }

  createTurn() {
    this.actualDart = 0;

    if (gameAlreadyStarted != 0) {
      this.createHTMLTurn();
      return;
    }

    let data = {
      "round_number": this.actualRound,
      "player_id": this.playerPlaying.id
    }
    requestManager.post("rooms/"+this.id+"/turns", data, (turnCreated) => {this.actualTurn = turnCreated.id; this.createHTMLTurn();});
  }

  createHTMLTurn() {
    if($("#playingArrows").length != 0) {
      $("#playingArrows").removeAttr("id");
    }
    let arrows="";
    for (let i=0; i<this.arrowsPerTurn; i++) {
      arrows += "<li class='arrow' style='width:"+100/this.arrowsPerTurn+"%"+"'>  </li>";
    }
    let arrowsContainer = "<ul id='playingArrows' class='arrows'>"+arrows+"</ul>";

    let HTMLTurn = $("<div id='turn_"+this.actualTurn+"' class='rowTurn'><div class='turnPlayerName'>"+this.playerPlaying.name+"</div>"+arrowsContainer+"<a class='modal-trigger' href='#modifyTurn' onclick='modifyTurn("+this.actualTurn+")'><i class='modifyTurn material-icons'>edit</i></a></div>");
    $("#actualRound").prepend(HTMLTurn);
  }

  createRound() {
    this.actualRound++;
    this.createHTMLRound();
  }

  createHTMLRound() {
    this.indexPlayerPlaying = 0;
    let round = $("<div id='actualRound'></div>");
    $("#turnsContainer").prepend(round);
    this.changePlayerPlaying();
  }

  updateArrowsLeftHTML() {
    $("#leftArrowsPart").empty();
    for (let i=0; i<this.arrowsLeft; i++) {
      let HTMLArrow = $("<li class='collection-item valign-wrapper arrowItem'><i class='material-icons circle icon-colored'>call_received</i></li>");
      $("#leftArrowsPart").append(HTMLArrow);
    }
  }
}

class Player {
  constructor(playerObject) {
    this.name = playerObject.name;
    this.id = playerObject.id;
  
    this.score = 0;
  
    let icon;
    (playerObject.iconName == undefined) ? icon = "account_circle" : icon = playerObject.iconName;
    let color;
    (playerObject.iconColor == undefined) ? color = "grey" : color = playerObject.iconColor;
    let HTMLItem = $("<li id='"+this.id+"' class='totalPlayer collection-item avatar valign-wrapper notActualPlayer'><i class='material-icons circle "+color+"'>"+icon+"</i><div class='playerPart'><span class='title playerPartItem'>"+playerObject.name+"</span><span class='title playerPartItem scorePlayer'>0</span></div></li>");
    $("#totalPlayerScore").append(HTMLItem);
  }
}


var roomManager;
// const requestManager = new LocalManager();
const requestManager = new RequestManager();

const myTarget = new Target();


function modifyTurn(turnID) {
  $("#modalInputContainer").empty();
  for (let i=0; i<roomManager.arrowsPerTurn; i++) {
    let arrowScore = $("#turn_"+turnID+" .arrow:nth-child("+(i+1)+")").html();
    let input = $("<div class='turnsInput input-field col s12'><input id='shot_"+i+"' placeholder="+Number(arrowScore)+" type='number' class='validate'></br><label class='active' for='shot_"+i+"'>Shot n°"+(i+1)+"</label></div>");
    $("#modalInputContainer").append(input);
  }
}

function showHideTurns() {
  $('#turnsContainer').toggle('isHidden');
  $('#hideTurns_btn').toggle('isHidden');
  $('#showTurns_btn').toggle('isHidden');
}

function showPlayers(id) {
  requestManager.get("rooms/"+id+"/players", (response) => {
    response.forEach(res => {
      roomManager.players.push(new Player(res.player));
    });
    roomManager.createRound();

    if (gameAlreadyStarted != 0) {
      getTurns();
    }
  },
  (r) => {console.log(r)});
}

function circleToScore(numberTouched, circle) {
  if (circle == null) {
    return 0;
  }
  const level = circle.level;

  switch (level) {
    case 0: return 50;
    case 1: return 25;
    case 2: return numberTouched;
    case 3: return numberTouched * 3;
    case 4: return numberTouched;
    case 5: return numberTouched * 2;
    default: return 0;
  }
}

function getRoom() {
  let added = "";
  if (gameAlreadyStarted != 0) {
    if (gameId == null) {
      added = "?status=Pending&sort=createdAt,desc"
    }
    else {
      added = "/"+gameId;
    }
  }
  else {
    added = "?status=Running";
  }

  requestManager.get("rooms"+added, (res) => {
    let response;
    
    (gameId == null) ? response = res[0] : response = res;
    console.log(response);
    roomManager = new RoomManager(response.id, response.room_config.arrows_per_turn, response.room_config.limit_points, response.room_config.limit_rounds);
    if (gameAlreadyStarted == 0) {
      let data = {
        "room_status": {"id": roomStatus.Pending}
      }
      console.log(roomStatus.Pending);
      requestManager.patch("rooms/"+response.id, data, () => {showPlayers(response.id)});
    }
    else {
      showPlayers(response.id);
    }
  },
  (r) => {location.href="index.html"});
}

function getTurns() {
  requestManager.get("rooms/"+roomManager.id+"/turns?sort=id&darts", (turns) => {
    turns.forEach(turn => {
      roomManager.actualTurn = turn.id;
      turn.darts.sort((a, b) => {
        if (a.dart_number < b.dart_number) {
          return -1;
        }
        if (a.dart_number > b.dart_number) {
          return 1;
        }
        return 0;
      })
      turn.darts.forEach(dart => {
        roomManager.scoreToHTML(dart.score);
        myTarget.shots.push({"posX": dart.position.x, "posY": dart.position.y});
      });
    });
    myTarget.drawShots();
    if (gameAlreadyStarted == 1) {
      gameAlreadyStarted = 0;
    }
    else if (gameAlreadyStarted == 2) {
      myTarget.isPlayable = false;
    }
  },

  (r) => {
    $("#turnsContainer").empty();
    if (gameAlreadyStarted == 1) {
      gameAlreadyStarted = 0;
      roomManager.createRound();
    }
    else if (gameAlreadyStarted == 2) {
      myTarget.isPlayable = false;
    }
  });
}



function endRoom() {
  let winner = null;
  let bestScore = 0;
  roomManager.players.forEach(player => {
    if (winner == null ||player.score > bestScore) {
      bestScore = player.score;
      winner = player;
    }
  });
  let data = {
    "room_status": {"id": roomStatus.Finished}
  }
  requestManager.patch("rooms/"+roomManager.id, data, () => {
    console.log(winner);
    let data = {
      "winner": {"id": winner.id}
    }
    requestManager.patch("rooms/"+roomManager.id, data, () => {location.href = "index.html"});
  });
}

function pauseRoom() {
  location.href = "index.html";
}


var gameAlreadyStarted = localStorage.getItem('continueGame');
var gameId = localStorage.getItem('gameId');
console.log(gameAlreadyStarted);




function main() {
  requestManager.get("roomstatus", (roomstatus) => {
    roomstatus.forEach(status => {
      if (status.status == "Running") {
        roomStatus.Running = status.id;
      }
      else if (status.status == "Pending") {
        roomStatus.Pending = status.id;
      }
      else if (status.status == "Finished") {
        roomStatus.Finished = status.id;
      }
    });
    getRoom();
  },
  (r) => {console.log(r)});
}

var roomStatus = {};
main();