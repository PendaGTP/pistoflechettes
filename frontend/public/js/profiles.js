//const requestManager = new LocalManager();
const requestManager = new RequestManager();

$(document).ready(function(){
    $('.modal').modal();
});

function createProfile() {
    let data = {"name": $("#newProfileName").val(), "regular": true};
    $("#newProfileName").val("");
    requestManager.post("players", data, showPlayers);
}

function modifyEditModal(pseudoItem, profileId) {
    $("#newNameProfile").attr("placeholder", pseudoItem.text());
    $("#newNameProfile").val("");
    document.getElementById("modalValidation_btn").onclick = function() {
        validateNewName(pseudoItem.text(), profileId);
    }
}

function modifyDeleteModal(pseudo, id) {
    $("#nameToDelete").text(pseudo);
    document.getElementById("deletePlayer").onclick = function() {
        requestManager.delete("players/"+id, showPlayers);
    }
}

function validateNewName(oldName, profileId) {
    let newName = $("#newNameProfile").val();

    if (oldName == newName) {return};

    let data = {"name": newName};
    
    requestManager.patch("players/"+profileId, data, showPlayers);
}

function profilesCreateHTML(profileObject) {
    let container = $("<li class='collection-item avatar'></li>");

    // picture part
    let pictureContainer = $("<div class='center-avatar'><i class='material-icons circle'>account_box</i></div>");
    container.append(pictureContainer);

    // profile infos part
    let pseudo = $("<span class='title'></span>").text(profileObject.name);
    let statContainer = $("<p>Win/Lose : <span>10/7</span></br> Dernière partie le <span>27/01/2019</span></p>");
    container.append(pseudo);
    container.append(statContainer);

    // actions part
    let actions = [["edit", "blue"], ["show_chart", "green"], ["history", "yellow"], ["clear", "red"]];
    let actionsContainer = $("<div class='secondary-content'></div>");
    actions.forEach(action => {
        let label = action[0];
        let color = action[1];
        let newAction = $("<a class='btn-floating "+color+"'><i class='material-icons'>"+label+"</i></a>");

        if (label == "edit") {
            newAction.addClass("modal-trigger");
            newAction.click(() => {
                modifyEditModal(pseudo, profileObject.id);
            });
            newAction.attr("href", "#editProfile");
        }

        else if (label == "clear") {
            newAction.addClass("modal-trigger");
            newAction.attr("href", "#deleteProfile");
            newAction.click(() => {
                modifyDeleteModal(pseudo.text(), profileObject.id);
            });
        }


        actionsContainer.append(newAction);
    });

    container.append(actionsContainer);

    return container;
}

function showPlayers() {
    requestManager.get("players", (response) => {console.log(response); appendPlayersToDOM(response)},(r) => {console.log(r)});
}

function appendPlayersToDOM(response) {
    $("#list_profiles").empty();
    response.forEach(data => {
        let dataHTML = profilesCreateHTML(data);
        $("#list_profiles").append(dataHTML);
    });
}

showPlayers();



