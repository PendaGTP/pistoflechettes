//const requestManager = new LocalManager();
const requestManager = new RequestManager();

function showHistoric() {
  requestManager.get("rooms?sort=createdAt,desc", (response) => {appendHistoricToDOM(response)},(r) => {console.log(r)});
}

function roomCreateHTML(roomObject) {
  let container = $("<li class='collection-item avatar'></li>");

  // icon part
  let iconColor;
  if (roomObject.room_status.status == "Pending" || roomObject.room_status.status == "Running") {
    iconColor = "orange";
  }
  else if (roomObject.room_status.status == "Finished") {
    iconColor = "green";
  }
  else {
    iconColor = "grey";
  }

  let iconLabel;
  if (roomObject.room_mode.mode == "Normal") {
    iconLabel = "gps_fixed";
  }
  else {
    iconLabel = "add";
  }

  let iconPart = $("<div class='center-avatar'><i class='material-icons circle "+iconColor+"'>"+iconLabel+"</i></div>")
  container.append(iconPart);



  // title
  let title = $("<span class='title'>"+roomObject.name+"</span>");
  container.append(title);

  // infos part
  console.log(roomObject);
  let winner = "???";
  let endDate = "???";
  if (roomObject.room_status.status == "Finished") {
    winner = roomObject.winner.name;
    let date = new Date(roomObject.created_at);
    endDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" +date.getFullYear()
  }
  let infos = $("<p>Winner : <span>"+winner+"</span></br> End: <span>"+endDate+"</span></p>");
  container.append(infos);

  // secondary contents
  let content = "";
  if (roomObject.room_status.status == "Pending" || roomObject.room_status.status == "Running") {
    let btn_continue = "<a class='btn-floating orange' onclick='openRoom("+roomObject.id+")'><i class='material-icons'>play_arrow</i></a>";
    content += btn_continue;
  }

  let btn_seeMore = "<a class='btn-floating' onclick='seeRoom("+roomObject.id+")'><i class='material-icons'>visibility</i></a>";
  content += btn_seeMore;
  
  let secondaryContent = $("<div class='secondary-content'>"+content+"</div>");
  container.append(secondaryContent);

  return container;
}

function appendHistoricToDOM(response) {
  console.log(response);
  $("#list_rooms").empty();
  response.forEach(data => {
      let dataHTML = roomCreateHTML(data);
      $("#list_rooms").append(dataHTML);
  });
}

function openRoom(roomId) {
  localStorage.setItem('continueGame', '1');
  localStorage.setItem('gameId', roomId);
  location.href='game.html';
}

function seeRoom(roomId) {
  localStorage.setItem('continueGame', '2');
  localStorage.setItem('gameId', roomId);
  location.href='game.html';
}

showHistoric();