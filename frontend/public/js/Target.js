const vw = window.innerWidth/100;
const vh = window.innerHeight/100;

class Circle {
  constructor(target, level, min, max, hasSections, primaryColor, secondaryColor=primaryColor) {
    this.target = target;
    this.level = level;
    this.min = min;
    this.max = max;
    this.primaryColor = primaryColor;
    this.secondaryColor = secondaryColor;
    this.hasSections = hasSections;

    (this.max == this.target.radius) ? this.isBlackPart = true : this.isBlackPart = false;
  }

  draw() {
    if (this.hasSections) {
      this.target.drawAllSections(this);
    }
    else {
      this.target.drawSection(0, Math.PI * 2, [this.min, this.max], this.primaryColor);
      if (this.isBlackPart) {
        this.target.writeNumbers();
      }
    }
  }

  highlight(teta1) {
    if (this.hasSections) {
      const teta2 = teta1 + Math.PI / (this.target.sections / 2);
      this.target.drawSection(teta1, teta2, [this.min, this.max], "yellow");
    }
    else {
      this.target.drawSection(0, Math.PI * 2, [this.min, this.max], "yellow");
    }

    setTimeout(() => {this.draw(); this.target.drawShots(); }, 500);
  }
}

class Target {
  constructor() {
    this.isPlayable = true;

    this.c = document.getElementById("canvas");
    this.ctx = this.c.getContext("2d");
    if (window.innerWidth >= window.innerHeight) {
      this.c.width = Math.min(80*vw, 100*vh);
      this.c.height = Math.min(80*vw, 100*vh);
    }
    else {
      this.c.width = Math.min(100*vw, 80*vh);
      this.c.height = Math.min(100*vw, 80*vh);
    }
    
    this.marginLeft = $("#canvas").offset().left;
    this.marginTop = $("#canvas").offset().top;

    this.pos = {
      left: this.marginLeft,
      top: this.marginTop,
      right: this.marginLeft+this.c.width,
      bottom: this.marginTop+this.c.height
    };

    this.ctx.lineWidth = 1;
    this.ctx.font = "20px Arial";
    this.ctx.textAlign = "center";

    this.centerX = this.c.width / 2;
    this.centerY = this.c.width / 2;
    this.radius = this.c.width / 2 - 10;



    (window.innerHeight > window.innerWidth) ? this.zoomUnit = vh : this.zoomUnit = vw;
    this.zoomCan = document.getElementById("targetZoom");
    this.zoomCtx = this.zoomCan.getContext("2d");
    this.imgZoomData = null;

    this.sections = 20;

    this.circles = [
      new Circle(this, 0, 0, this.radius/20, false, "red"),
      new Circle(this, 1, this.radius/20 + 1, this.radius/8, false, "green"),
      new Circle(this, 2, this.radius/8 + 1, this.radius/2.5, true, "black", "white"),
      new Circle(this, 3, this.radius/2.5 + 1, this.radius/2.2, true, "red", "green"),
      new Circle(this, 4, this.radius/2.2 + 1, this.radius/1.3, true, "black", "white"),
      new Circle(this, 5, this.radius/1.3 + 1, this.radius/1.2, true, "red", "green"),
      new Circle(this, 6, this.radius/1.2 + 1, this.radius, false, "black")
    ]
    this.shots = [];

    this.order_numbers = [6, 10, 15, 2, 17, 3, 19, 7, 16, 8, 11, 14, 9, 12, 5, 20, 1, 18, 4, 13];
  
    this.drawTarget();

    this.c.addEventListener("touchstart", (e) => {
      if (!this.isPlayable) {
        return;
      }
      $("#targetZoom").css("display", "block");
      this.updateZoom(e);
    });
  
    this.c.addEventListener("touchmove", (e) => {
      if (!this.isPlayable) {
        return;
      }
      this.updateZoom(e);
    });

    this.c.addEventListener('touchend', (e) => {
      if (!this.isPlayable) {
        return;
      }
      var lastX = e.changedTouches[0].pageX;
      var lastY = e.changedTouches[0].pageY;
  
      if (lastX < this.pos.left || lastX > this.pos.right || lastY < this.pos.top || lastY > this.pos.bottom) {
        $("#targetZoom").css("display", "none");
        console.log("no action");
        return;
      }
  
      $("#targetZoom").css("display", "none");
      this.clickTarget(e);
    });
  }

  updateZoom(e) {
    let posX = e.changedTouches[0].clientX - this.marginLeft;
    let posY = e.changedTouches[0].clientY - this.marginTop;

    $("#targetZoom").css("top", (posY+this.marginTop-5*this.zoomUnit)+"px");
    $("#targetZoom").css("left", (posX+this.marginLeft-5*this.zoomUnit)+"px");

    this.imgZoomData = this.ctx.getImageData(posX - 2 * vw, posY - 2 * vw, 4*vw, 4*vw);
    this.drawZoom(this.imgZoomData);
  }

  drawZoom(img) {
    let pixel_width = this.zoomCan.width / img.width;
    let pixel_height = this.zoomCan.height / img.height;
  
    let pixel_i = 0;
    let r,g,b;
    for (let i=0; i<img.height; i++) {
      for (let j=0; j<img.width; j++) {
        this.zoomCtx.beginPath();
        this.zoomCtx.fillStyle = "rgba("+r+","+g+","+b+",255)";
        this.zoomCtx.rect(j*pixel_width, i*pixel_height, pixel_width, pixel_height);
        r = img.data[pixel_i];
        g = img.data[pixel_i + 1];
        b = img.data[pixel_i + 2];
        this.zoomCtx.fill();
        this.zoomCtx.closePath();
        pixel_i += 4;
      }
    }
  }

  drawShots() {
    for (let i=0; i<this.shots.length; i++) {
      this.ctx.beginPath();
      this.ctx.arc(this.shots[i].posX, this.shots[i].posY, 5, 0, 2*Math.PI);
      this.ctx.closePath();
      this.ctx.fillStyle = "yellow";
      this.ctx.fill();
    }
  }

  drawTarget() {
    for (let i=0; i<this.circles.length; i++) {
      this.circles[i].draw();
    }
  }

  drawAllSections(circle) {
    let teta1 = - Math.PI / this.sections;
    let teta2;
    for (let i=1; i<this.sections+1; i++) {
      teta2 = teta1 + Math.PI / (this.sections / 2);
      let color;
      (i%2==0) ? color=circle.primaryColor : color=circle.secondaryColor;
      this.drawSection(teta1, teta2, [circle.min, circle.max], color);
      teta1 = teta2;
    }
  }

  drawSection(teta1, teta2, valuesMinMax, middleFillColor, strokeColor=null) {
    let min, max, x1, x2, x3, y1, y2, y3;
    max = valuesMinMax[1];
    min = valuesMinMax[0];

    this.ctx.beginPath();
    x1 = this.centerX + max * Math.cos(teta1);
    y1 = this.centerY + max * Math.sin(teta1);
    x2 = this.centerX + min * Math.cos(teta1);
    y2 = this.centerY + min * Math.sin(teta1);
    x3 = this.centerX + min * Math.cos(teta2);
    y3 = this.centerY + min * Math.sin(teta2);

    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.arc(this.centerX, this.centerY, min, teta1, teta2, false);
    this.ctx.lineTo(x3, y3);
    this.ctx.arc(this.centerX, this.centerY, max, teta2, teta1, true);

    this.ctx.closePath();

    (strokeColor!=null) ? this.ctx.strokeStyle = strokeColor : this.ctx.strokeStyle = middleFillColor;
    this.ctx.stroke();

    this.ctx.fillStyle = middleFillColor;
    this.ctx.fill();
  }

  writeNumbers() {
    let msg, measure, fontWidth, fontHeight, txt_centerX, txt_centerY;
    const blackPartCircle = this.circles[this.circles.length-1];
    this.ctx.fillStyle = "white";
    let teta1 = - Math.PI / this.sections;
    let teta2;
    for (let i=1; i<this.sections+1; i++) {
      teta2 = teta1 + Math.PI / (this.sections / 2);
      msg = this.order_numbers[(this.sections-i+6)%(this.sections)];
      measure = this.ctx.measureText(msg);
      fontWidth = measure.actualBoundingBoxRight - measure.actualBoundingBoxLeft;
      fontHeight = measure.actualBoundingBoxAscent + measure.actualBoundingBoxDescent;
      txt_centerX = this.centerX + (blackPartCircle.min + (blackPartCircle.max - blackPartCircle.min)/2) * Math.sin((teta2+teta1)/2);
      txt_centerY = this.centerY + (blackPartCircle.min + (blackPartCircle.max - blackPartCircle.min)/2) * Math.cos((teta2+teta1)/2);
      this.ctx.fillText(msg, txt_centerX-fontWidth/2, txt_centerY+fontHeight/2);
      teta1 = teta2;
    }
  }

  getClickInfos(x, y) {
    let teta = Math.atan2(y - this.centerY, x - this.centerX) + Math.PI / this.sections;
    while (teta < 0) {teta += Math.PI * 2;}
  
    let acc = 0;
    while (acc*Math.PI / (this.sections / 2) < teta) {acc++;}
  
    let circleTouched = null;
    const distance = Math.sqrt((x-this.centerX)*(x-this.centerX) + (y-this.centerY)*(y-this.centerY));

    for (let i=0; i<this.circles.length; i++) {
      if (distance < this.circles[i].max) {
        circleTouched = this.circles[i];
        break;
      }
    }
    return {circleTouched: circleTouched, numberSection: acc, numberTouched: this.order_numbers[acc-1]};
  }

  clickTarget(e) {
    const posX = e.changedTouches[0].clientX - this.pos.left;
    const posY = e.changedTouches[0].clientY - this.pos.top;

    this.shots.push({"posX": posX, "posY": posY});
    this.ctx.beginPath();
    this.ctx.arc(posX, posY, 5, 0, 2*Math.PI);
    this.ctx.closePath();
    this.ctx.fillStyle = "yellow";
    this.ctx.fill();

    const clickInfos = this.getClickInfos(posX, posY);
    
    const numberTouched = clickInfos.numberTouched;
    const numberSection = clickInfos.numberSection;
    const circleTouched = clickInfos.circleTouched;


    const teta1 = - Math.PI / this.sections + (numberSection-1)* (Math.PI / (myTarget.sections / 2));

    if (circleTouched != null) {
      circleTouched.highlight(teta1);
    }

    addPointToPlayer(numberTouched, circleTouched, posX, posY);
  }
}