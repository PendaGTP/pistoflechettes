//const requestManager = new LocalManager();
const requestManager = new RequestManager();

var idsPlayersPlaying = [];

$(document).ready(function(){
    $('.modal').modal();
});

function createProfile() {
    let data = {"name": $("#newProfileName").val(), "regular": false};
    $("#newProfileName").val("");
    requestManager.post("players", data, showPlayers);
}

function playersCreateHTML(playerObject) {
    let playerButton = $("<a id='btnPlayer-"+playerObject.id+"' class='waves-effect waves-light light-blue lighten-1 btn' onclick='togglePlayerPlaying("+playerObject.id+")'>"+playerObject.name+"</a>");
    return playerButton;
}

function togglePlayerPlaying(idPlayer) {
    (idsPlayersPlaying.includes(idPlayer)) ? idsPlayersPlaying.splice(idsPlayersPlaying.indexOf(idPlayer), 1) : idsPlayersPlaying.push(idPlayer);

    $("#btnPlayer-"+idPlayer).toggleClass("light-blue lighten-1");
    $("#btnPlayer-"+idPlayer).toggleClass("green");
}


function showHide_rangeField(rangeContainer_id, rangeInput_id, labelValue_id) {
    let container = $("#"+rangeContainer_id);
    if(container.hasClass("isHidden")) {
        container.removeClass("isHidden");
        changeLabelValue($("#"+rangeInput_id).val(), labelValue_id);
    }
    else {
        container.addClass("isHidden");
        changeLabelValue("Unlimited", labelValue_id);
    }
}


function changeLabelValue(value, rangeValue_id) {
    $("#"+rangeValue_id).text(value);
}

function showPlayers() {
    requestManager.get("players", (response) => {appendPlayersToDOM(response)},(r) => {console.log(r)});
}

function appendPlayersToDOM(response) {
    $("#player-selection").empty();
    response.forEach(player => {
        let dataHTML = playersCreateHTML(player);
        let data = {"name": player.id};
        requestManager.post("teams", data, () => {$("#player-selection").append(dataHTML);})
    });
}

function launchGame() {
    if (!idsPlayersPlaying.length) {
        alert("You must choose at least one player.");
        return;
    }
    let arrowsPerTurn = $("#arrowPerTurn_labelValue").text();
    let limitPoints = $("#pointsLimit_labelValue").text();
    let limitRounds = $("#roundsLimit_labelValue").text();
    if (limitPoints == "Unlimited") {
        limitPoints = 0;
    }
    if (limitRounds == "Unlimited") {
        limitRounds = 0;
    }

    let roomConfig = {
        "arrows_per_turn": arrowsPerTurn,
        "limit_points": limitPoints,
        "limit_rounds": limitRounds
    }

    let playerFormList = [];
    idsPlayersPlaying.forEach(playerId => {
        playerFormList.push({"id": playerId, "team_id": playerId});
    });

    let roomName = $("#roomTitleInput").val();
    if (!roomName.length) {roomName = "NoName"};
    let data = {
        "name": roomName,
        "room_mode_id": 1,
        "players": playerFormList,
        "room_config": roomConfig
    }
    requestManager.post("rooms", data, () => {location.href = "game.html"});
}

showPlayers();