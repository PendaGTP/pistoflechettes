class RequestManager {
  constructor() {
    this.apiUrl = 'http://localhost:80/api/';
  }
  
  get(ressource, callback, callbackError) {
    $.ajax({
      "url": this.apiUrl + ressource,
      "type": "GET",
      "success": (r) => {callback(r)},
      "error": (r) => {callbackError(r)}
    });
  }

  patch(ressource, data, callback) {
    $.ajax({
      "url": this.apiUrl + ressource,
      "type": "PATCH",
      "data": JSON.stringify(data),
      "headers" : {
          "Content-Type" : "application/merge-patch+json"
      },
      "success": (r) => {callback(r)},
      "error": (r) => {console.log(r);}
    });
  }

  post(ressource, data, callback) {
    $.ajax({
      "url": this.apiUrl + ressource,
      "type": "POST",
      "data": JSON.stringify(data),
      "headers" : {
          "Content-Type" : "application/json"
      },
      "success": (r) => {callback(r)},
      "error": (r) => {console.log(r);}
    });
  }

  delete(ressource, callback) {
    $.ajax({
      "url": this.apiUrl + ressource,
      "type": "DELETE",
      "headers" : {
          "Content-Type" : "application/json"
      },
      "success": (r) => {callback(r)},
      "error": (r) => {console.log(r);}
    });
  }
}


class LocalManager {
  get(ressource, callback) {
    let res = ressource.split("/");
    let begin = res[0];
    let end = res[res.length-1];
    console.log(end);
    if (end == "rooms?status=Pending") {
      let response = [{
        "id": 1,
        "name": "localGame",
        "room_config": {
          "arrows_per_turn": 3,
          "limit_points": 0,
          "limit_rounds": 0
        }
      }];
      callback(response);
    }
    else if (end == "players" && begin == "rooms") {
      let response = [
        {"player": {"id": 1, "name": "Antoine", "iconColor": "blue", "iconName": "accessibility"}},
        {"player": {"id": 2, "name": "Clement", "iconColor": "red", "iconName": "account_circle"}},
        {"player": {"id": 3, "name": "Lepab", "iconColor": "orange", "iconName": "android"}},
      ]
      callback(response);
    }
    else if (end == "players") {
      let response = [
        {"id": 1, "name": "Antoineee"},
        {"id": 2, "name": "Clem"},
        {"id": 3, "name": "A"}
      ]
      callback(response);
    }
    else if (end == "players/playing") {
      let response = [
        {"name": "Antoineee"},
        {"name": "Clem"},
        {"name": "A"}
      ]
      callback(response);
    }
  }

  patch(ressource, data, callback) {
    console.log("LOCAL -> no action");
    callback();
  }

  post(ressource, data, callback) {
    console.log("LOCAL -> no action");
    callback();
  }

  delete(ressource, callback) {
    console.log("LOCAL -> no action");
    callback();
  }
}