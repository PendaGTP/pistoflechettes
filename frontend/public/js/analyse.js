/* Configuration des charts*/

const GLOBAL_CHART_OPTIONS = {
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
};

const HIGHTSCORE_CHART_CONFIG = {
  x: "name",
  y: "id",
  config: {
    type: "bar",
    data: {
      datasets: [
        {
          label: "HighScore",
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 2,
        },
      ],
    },
  },
};

/* Selection de l'analyse */
$("#selectForm").change(function () {
    let configChart;
    option = $(this).val();
    switch (option) {
      case "1":
        console.log("Changement de mode");
        configChart = { ...HIGHTSCORE_CHART_CONFIG, ...GLOBAL_CHART_OPTIONS };
        requestManager.get("players?sort=id,desc", (response) => {
          chartManager2.changeChart(configChart, response);
        });
        break;
  
      case "2":
        console.log("Changement de mode");
        configChart = { ...HIGHTSCORE_CHART_CONFIG, ...GLOBAL_CHART_OPTIONS };
        requestManager.get("teams?sort=id,asc", (response) => {
          chartManager2.changeChart(configChart, response);
        });
        break;
  
      case "3":
        console.log("Select mode 3");
        break;
  
      default:
        break;
    }
  });


// Utilisé pour create/delete/update un chart
class ChartManager {
  constructor() {
    this.canvasCtx = document.getElementById("chart").getContext("2d");
    this.chart = new Chart(this.canvasCtx);
  }

  changeChart(configChart, data) {
    this.chart.destroy();

    const labels = data.map((o) => o[configChart.x]);
    const values = data.map((o) => o[configChart.y]);
    const config = configChart.config;
    const options = configChart.options;

    config.data.datasets[0].data = values;
    config.data.labels = labels;

    this.chart = new Chart(this.canvasCtx);
    this.chart.config = config;
    this.chart.options = options;

    this.chart.update();
  }

  getData($ressourceUrl) {
    requestManager.get(ressourceUrl, callback);
  }
}

const selectFrom = $("select");
const instances = M.FormSelect.init(selectFrom);

const requestManager = new RequestManager();
const chartManager2 = new ChartManager();

// set default graph
requestManager.get("players?sort=id,desc", (response) => {
    chartManager2.changeChart(
      { ...HIGHTSCORE_CHART_CONFIG, ...GLOBAL_CHART_OPTIONS },
      response
    );
  });
