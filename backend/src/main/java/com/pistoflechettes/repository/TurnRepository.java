package com.pistoflechettes.repository;

import com.pistoflechettes.model.Turn;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TurnRepository extends CrudRepository<Turn, Long> {
    List<Turn> findAll(Specification<Turn> spec, Sort sort);

    List<Turn> findAllByRoomId(Long roomId, Sort sort);
}
