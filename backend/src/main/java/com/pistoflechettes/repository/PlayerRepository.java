package com.pistoflechettes.repository;

import com.pistoflechettes.model.player.Player;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    List<Player> findAll(Specification<Player> spec, Sort sortBy);

    Optional<Player> findAllByRegularIsTrueAndName(String name);
}
