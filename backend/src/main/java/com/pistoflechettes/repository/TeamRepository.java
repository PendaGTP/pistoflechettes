package com.pistoflechettes.repository;

import com.pistoflechettes.model.Team;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team, Long> {
    List<Team> findAll(Specification<Team> spec, Sort sortBy);
}
