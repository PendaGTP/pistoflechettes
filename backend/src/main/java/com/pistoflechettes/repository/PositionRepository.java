package com.pistoflechettes.repository;

import com.pistoflechettes.model.score.Position;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PositionRepository extends CrudRepository<Position, Long> {
    @Override
    List<Position> findAll();

    Optional<Position> findByXAndAndYAndDepth(float x, float y, Long depth);
}
