package com.pistoflechettes.repository;

import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.model.room.Room;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room, Long> {
    List<Room> findAll(Specification<Room> spec, Sort sortBy);

    Optional<Room> findByRoomStatus(RoomStatus roomStatus);


}
