package com.pistoflechettes.repository;

import com.pistoflechettes.enums.Status;
import com.pistoflechettes.model.RoomStatus;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoomStatusRepository extends CrudRepository<RoomStatus, Long> {
    List<RoomStatus> findAll(Specification<RoomStatus> spec, Sort sortBy);

    Optional<RoomStatus> findByStatus(Status status);
}