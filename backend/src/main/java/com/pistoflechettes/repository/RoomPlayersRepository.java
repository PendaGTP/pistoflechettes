package com.pistoflechettes.repository;

import com.pistoflechettes.model.RoomPlayers;
import com.pistoflechettes.model.room.Room;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RoomPlayersRepository extends PagingAndSortingRepository<RoomPlayers, Long> {
    @Override
    List<RoomPlayers> findAll();

    List<RoomPlayers> findAllByRoom(Room r, Sort sortBy);
}
