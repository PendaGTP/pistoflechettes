package com.pistoflechettes.repository;

import com.pistoflechettes.enums.Mode;
import com.pistoflechettes.model.RoomMode;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RoomModeRepository extends CrudRepository<RoomMode, Long> {
    List<RoomMode> findAll(Specification<RoomMode> spec, Sort sort);

    Optional<RoomMode> findByMode(Mode roomMode);
}
