package com.pistoflechettes.repository;

import com.pistoflechettes.model.score.Dart;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DartRepository extends CrudRepository<Dart, Long> {
    @Override
    List<Dart> findAll();
}
