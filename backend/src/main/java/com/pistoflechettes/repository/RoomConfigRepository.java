package com.pistoflechettes.repository;

import com.pistoflechettes.model.RoomConfig;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoomConfigRepository extends CrudRepository<RoomConfig, Long> {
    @Override
    List<RoomConfig> findAll();

    RoomConfig findByArrowsPerTurnAndLimitPointsAndLimitRounds(long arrowsPerTurn, long limitPoints, long limitRounds);
}
