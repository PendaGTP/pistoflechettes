package com.pistoflechettes.model.player;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor()
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
public class PlayerDTO {

    Long id;
    String name;
    Boolean regular;

}
