package com.pistoflechettes.model.player;

import com.pistoflechettes.model.RoomPlayers;
import com.pistoflechettes.model.Turn;
import com.pistoflechettes.model.room.Room;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level= AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class Player implements  Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    Long id;

    @NotBlank
    @Size(max = 200)
    @Column(nullable=false)
    @Getter
    @Setter
    String name;

    @NotNull
    @Getter
    @Setter
    Boolean regular = false;


    @OneToMany(mappedBy = "player", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private final Set<RoomPlayers> roomPlayers = new HashSet<>(); //Player <-> RoomsPlayers OneToMany

    @OneToMany(mappedBy = "player", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private final Set<Turn> turns = new HashSet<>(); //Player <-> Turn OneToMany

    @OneToMany(mappedBy = "winnerId", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private final Set<Room> winnerId = new HashSet<>(); //Player <-> Turn OneToMany

}
