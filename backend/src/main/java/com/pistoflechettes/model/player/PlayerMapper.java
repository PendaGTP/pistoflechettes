package com.pistoflechettes.model.player;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PlayerMapper {

    // L'injection de dépendance ne semble pas fonctionner alors j'utilise la factory pour me retourner l'instance
    PlayerMapper INSTANCE = Mappers.getMapper(PlayerMapper.class);

    PlayerDTO toPlayerDTO(Player player);

    List<PlayerDTO> toPlayerDTOs(List<Player> players);

    Player toPlayer(PlayerDTO playerDTO);
}
