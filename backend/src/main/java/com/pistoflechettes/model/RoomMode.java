package com.pistoflechettes.model;

import com.pistoflechettes.enums.Mode;
import com.pistoflechettes.model.room.Room;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class RoomMode implements Serializable {

    @OneToMany(mappedBy = "roomMode", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    final
    Set<Room> rooms = new HashSet<>(); //RoomMode <-> Room OneToMany
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(unique = true)
    @Getter
    @Setter
    Mode mode;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    Long id;

    public RoomMode(Mode mode) {
        this.mode = mode;
    }

}
