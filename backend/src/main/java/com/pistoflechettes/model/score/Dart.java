package com.pistoflechettes.model.score;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.Turn;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class Dart implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    long id;

    @ManyToOne
    @JoinColumn(name = "pos_id", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE) //Dart <-> Position  Many to one
    @Getter
    @Setter
    Position position;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE) //Dart <-> Turn  Many to one
            Turn turn;

    @JsonProperty("dart_number")
    @Getter
    @Setter
    Long dartNumber;

    @Getter
    @Setter
    Long label; //1,2,3,...

    @Getter
    @Setter
    Long score; //1*2,10*3


    public Dart(Long score, Position position, Turn turn, Long label, Long dartNumber) {
        this.score = score;
        this.position = position;
        this.turn = turn;
        this.label = label;
        this.dartNumber = dartNumber;
    }
}
