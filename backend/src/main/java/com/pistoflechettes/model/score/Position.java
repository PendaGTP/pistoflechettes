package com.pistoflechettes.model.score;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"x", "y", "depth"})
)
@Entity
public class Position implements Serializable {

    @OneToMany(mappedBy = "position", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    final Set<Dart> darts = new HashSet<>(); //RoomDate <-> Room OneToMany

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotNull
    Float x;
    @NotNull
    Float y;
    @NotNull
    Long depth;

    public Position() {
    } //JPA constructor

    public Position(Float x, Float y, Long depth) {
        this.x = x;
        this.y = y;
        this.depth = depth;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Long getDepth() {
        return depth;
    }

    public void setDepth(Long depth) {
        this.depth = depth;
    }
}
