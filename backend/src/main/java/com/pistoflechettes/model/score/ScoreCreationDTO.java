package com.pistoflechettes.model.score;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class ScoreCreationDTO {
    @JsonProperty("dart_number")
    Long dartNumber;
    Long label;
    Long score;
    PositionDTO position;
}
