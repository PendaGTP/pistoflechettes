package com.pistoflechettes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.room.Room;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class RoomPlayers implements Serializable, Comparable<RoomPlayers> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    long id;

    @ManyToOne(cascade = CascadeType.REMOVE) //RoomPlayers <-> Player  Many to one
    @Getter
    @Setter
    Player player;

    @ManyToOne(cascade = CascadeType.REMOVE) //RoomPlayers <-> Team  Many to one
    @Getter
    @Setter
    Team team;

    @ManyToOne(cascade = CascadeType.REMOVE) //RoomPlayers <-> Room  Many to one
    @Setter
    Room room;

    @JsonProperty("player_order")
    @Getter
    @Setter
    Long playerOrder;

    // hex color
    @JsonProperty("player_color")
    @Getter
    @Setter
    String playerColor;


    public RoomPlayers(Player player, Team team, Room room, Long playerOrder) {
        this.player = player;
        this.team = team;
        this.room = room;
        this.playerOrder = playerOrder;
    }

    @Override
    public int compareTo(RoomPlayers o) {
        System.out.println("THIS :" + this.getPlayerOrder());
        System.out.println("COMPARE WITH :" + o.getPlayerOrder());
        System.out.println("RESULT : " + this.getPlayerOrder().compareTo(o.getPlayerOrder()));

        return this.getPlayerOrder().compareTo(o.getPlayerOrder());
    }
}
