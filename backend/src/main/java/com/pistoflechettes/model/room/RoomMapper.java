package com.pistoflechettes.model.room;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoomMapper {

    // L'injection de dépendance ne semble pas fonctionner alors j'utilise la factory pour me retourner l'instance
    RoomMapper INSTANCE = Mappers.getMapper(RoomMapper.class);

    // Convert RoomCreationDTO
    RoomCreationDTO toRoomCreationDTO(Room room);
    List<RoomCreationDTO> toRoomCreationDTOs(List<Room> rooms);
    Room toRoom(RoomCreationDTO roomCreationDTO);

    // Convert RoomDTO
    RoomDTO toRoomDTO(Room room);
    List<RoomDTO> toRoomDTOs(List<Room> rooms);
    Room fromRoomDTOtoRoom(RoomDTO roomDTO);
}
