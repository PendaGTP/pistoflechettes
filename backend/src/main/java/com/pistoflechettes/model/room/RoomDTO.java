package com.pistoflechettes.model.room;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.RoomConfig;
import com.pistoflechettes.model.RoomMode;
import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.player.PlayerDTO;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.List;

@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
public class RoomDTO {

    Long id;

    String name;

    List<PlayerDTO> players;

    @JsonProperty("room_status")
    RoomStatus roomStatus;

    @JsonProperty("created_at")
    Date createdAt;

    @JsonProperty("end_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    Date endAt;

    @JsonProperty("room_mode")
    RoomMode roomMode;

    @JsonProperty("winner")
    Player winnerId;

    @JsonProperty("room_config")
    RoomConfig roomConfig;

}