package com.pistoflechettes.model.room;

import com.pistoflechettes.form.PlayerForm;
import com.pistoflechettes.model.RoomConfig;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter
@Setter
public class RoomCreationDTO {

    @NotEmpty
    String name;
    @NotNull
    Long room_mode_id;
    @NotNull
    List<PlayerForm> players;
    @NotNull
    RoomConfig room_config;

}
