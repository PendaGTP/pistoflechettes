package com.pistoflechettes.model.room;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.*;
import com.pistoflechettes.model.player.Player;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level= AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Room implements Serializable {

    @OneToMany(mappedBy = "room", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    @Getter
    private final Set<RoomPlayers> roomPlayers = new HashSet<>(); //Room <-> RoomsPlayers OneToMany

    @OneToMany(mappedBy = "room", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    @Getter
    private final Set<Turn> turns = new HashSet<>(); //Room <-> RoomsPlayers OneToMany

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    Long id;

    @NotNull
    @Size(max = 200)
    @Getter @Setter
    String name;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE) //Room <-> RoomStatus  Many to one
    @Getter @Setter
    @JsonProperty("room_status")
    RoomStatus roomStatus;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE) //Room <-> RoomMode  Many to one
    @Getter @Setter
    @JsonProperty("room_mode")
    RoomMode roomMode;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE) //Room <-> Player  Many to one
    @Getter
    @Setter
    @JsonProperty("winner")
    Player winnerId;

    @ManyToOne // Room <-> RoomConfig Many to one
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Getter
    @Setter
    @JsonProperty("room_config")
    RoomConfig roomConfig;

    @CreatedDate
    @JsonProperty("created_at")
    @Getter
    @Setter
    Date createdAt;

    @JsonProperty("end_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    @Getter
    @Setter
    String endAt;

}

