package com.pistoflechettes.model;

import com.pistoflechettes.enums.Status;
import com.pistoflechettes.model.room.Room;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class RoomStatus implements Serializable {

    @OneToMany(mappedBy = "roomStatus", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    final
    Set<Room> rooms = new HashSet<>(); //RoomStatus <-> Room OneToMany
    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(unique = true)
    @Getter
    @Setter
    Status status;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    Long id;

    public RoomStatus(Status status) {
        this.status = status;
    }

}
