package com.pistoflechettes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.room.Room;
import com.pistoflechettes.model.score.Dart;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class Turn implements Serializable {

    @OneToMany(mappedBy = "turn", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    @Getter
    @Setter
    Set<Dart> darts = new HashSet<>(); //Turn <-> Dart OneToMany

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    Long id;

    @NotNull
    @JsonProperty("round_number")
    @Getter
    @Setter
    Long roundNumber;

    @ManyToOne(cascade = CascadeType.REMOVE) //Room <-> Turn  Many to one
    @Setter
    Room room;

    @ManyToOne(cascade = CascadeType.PERSIST) //Room <-> Turn  Many to one
    @Getter
    @Setter
    Player player;

    @Getter
    @Setter
    Long score;

}
