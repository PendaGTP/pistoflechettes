package com.pistoflechettes.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pistoflechettes.model.room.Room;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class RoomConfig implements Serializable {

    @OneToMany(mappedBy = "roomConfig", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    final
    Set<Room> rooms = new HashSet<>(); // RoomConfig <-> Room OneToMany

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    Long id;

    @JsonProperty("arrows_per_turn")
    @Getter
    @Setter
    Long arrowsPerTurn;

    @JsonProperty("limit_points")
    @Getter
    @Setter
    Long limitPoints;

    @JsonProperty("limit_rounds")
    @Getter
    @Setter
    Long limitRounds;

}
