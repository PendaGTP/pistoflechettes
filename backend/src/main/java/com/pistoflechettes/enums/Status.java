package com.pistoflechettes.enums;

public enum Status {
    Running, Finished, Pending
}
