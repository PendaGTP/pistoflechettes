package com.pistoflechettes.exception;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(Class clazz, String... searchParamsMap) {
        super(EntityNotFoundException.generateMessageEntity(clazz.getSimpleName(), toMap(searchParamsMap)));
    }

    public EntityNotFoundException(Class clazz) {
        super(EntityNotFoundException.generateMessageEntities(clazz.getSimpleName()));
    }

    private static String generateMessageEntity(String entity, Map<String, String> searchParams) {
        return StringUtils.capitalize(entity) +
                " was not found for parameters " +
                searchParams;
    }

    private static String generateMessageEntities(String entity) {
        return StringUtils.capitalize(entity) +
                "(s) were not found ";
    }

    private static <K, V> Map<K, V> toMap(
            Object... entries) {
        if (entries.length % 2 == 1)
            throw new IllegalArgumentException("Invalid entries");
        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(((Class<K>) String.class).cast(entries[i]), ((Class<V>) String.class).cast(entries[i + 1])),
                        Map::putAll);
    }
}
