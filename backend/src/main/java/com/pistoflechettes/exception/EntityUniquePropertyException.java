package com.pistoflechettes.exception;

public class EntityUniquePropertyException extends RuntimeException {
    public EntityUniquePropertyException(Class entity, String property, String value) {
        super("Property <" + property + "> with value <" + value + "> of " + entity.getSimpleName() + " is already used, please try another value");
    }
}
