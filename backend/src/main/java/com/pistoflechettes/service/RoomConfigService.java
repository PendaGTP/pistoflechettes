package com.pistoflechettes.service;

import com.pistoflechettes.model.RoomConfig;
import com.pistoflechettes.repository.RoomConfigRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomConfigService {

    private final RoomConfigRepository roomConfigRepository;

    public RoomConfig addRoomConfig(RoomConfig newRoomConfig) {
        RoomConfig roomConfig = roomConfigRepository.findByArrowsPerTurnAndLimitPointsAndLimitRounds(newRoomConfig.getArrowsPerTurn(), newRoomConfig.getLimitPoints(), newRoomConfig.getLimitRounds());
        if (roomConfig == null) {
            return roomConfigRepository.save(newRoomConfig);
        }
        return roomConfig;
    }

    public RoomConfig getRoomConfig(long id) {
        Optional<RoomConfig> roomConfigOptional = roomConfigRepository.findById(id);
        return roomConfigOptional.orElse(null);
    }
}
