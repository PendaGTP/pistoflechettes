package com.pistoflechettes.service;

import com.pistoflechettes.enums.Status;
import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.repository.RoomStatusRepository;
import com.pistoflechettes.utils.ApiUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.json.JsonMergePatch;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomStatusService {

    private final ApiUtils apiUtils;
    private final RoomStatusRepository roomStatusRepository;


    public RoomStatus addRoomStatus(RoomStatus roomStatus) {
        Optional<RoomStatus> roomStatusOptional = roomStatusRepository.findByStatus(roomStatus.getStatus());
        return roomStatusOptional.orElseGet(() -> roomStatusRepository.save(roomStatus));
    }

    public List<RoomStatus> all(Specification<RoomStatus> spec, Sort sort) {
        List<RoomStatus> roomStatusList = roomStatusRepository.findAll(spec, sort);
        if (roomStatusList.isEmpty()) {
            throw new EntityNotFoundException(RoomStatus.class);
        }
        return roomStatusList;
    }

    public RoomStatus getRoomStatus(Long id) {
        Optional<RoomStatus> roomStatusOptional = roomStatusRepository.findById(id);
        if (!roomStatusOptional.isPresent()) {
            throw new EntityNotFoundException(RoomStatus.class, "id", id.toString());
        }
        return roomStatusOptional.get();
    }

    public RoomStatus updateRoomStatus(Long id, JsonMergePatch mergePatchDocument) {
        RoomStatus roomStatus = getRoomStatus(id);
        RoomStatus roomStatusPatched = apiUtils.mergePatch(mergePatchDocument, roomStatus, RoomStatus.class);
        return roomStatusRepository.save(roomStatusPatched);
    }

    public void deleteAllRoomStatus() {
        roomStatusRepository.deleteAll();
    }

    public void deleteRoomStatus(Long id) {
        roomStatusRepository.deleteById(getRoomStatus(id).getId());
    }

    public RoomStatus getRoomStatusByStatus(Status status) {
        Optional<RoomStatus> roomStatusOptional = roomStatusRepository.findByStatus(status);
        return roomStatusOptional.orElse(null);
    }

}
