package com.pistoflechettes.service;

import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.exception.EntityUniquePropertyException;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.repository.PlayerRepository;
import com.pistoflechettes.utils.ApiUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.json.JsonMergePatch;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PlayerService {

    private final ApiUtils apiUtils;
    private final PlayerRepository playerRepository;

    public Player addPlayer(Player player) {
        if (player.getRegular()) {
            playerRepository.findAllByRegularIsTrueAndName(player.getName()).ifPresent(
                    existingUser -> {
                        throw new EntityUniquePropertyException(Player.class, "name", existingUser.getName());
                    });
        }
        return playerRepository.save(player);
    }

    public List<Player> all(Specification<Player> spec, Sort sort) {
        List<Player> players = playerRepository.findAll(spec, sort);
        if (players.isEmpty()) {
            throw new EntityNotFoundException(Player.class);
        }
        return players;
    }

    public Player getPlayer(Long id) {
        Optional<Player> playerOptional = playerRepository.findById(id);

        if (!playerOptional.isPresent()) {
            throw new EntityNotFoundException(Player.class, "id", id.toString());
        }

        return playerOptional.get();
    }

    public Player updatePlayer(Long id, JsonMergePatch mergePatchDocument) {
        Player player = getPlayer(id);
        Player playerPatched = apiUtils.mergePatch(mergePatchDocument, player, Player.class);
        if (playerPatched.getRegular()) {
            playerRepository.findAllByRegularIsTrueAndName(playerPatched.getName()).ifPresent(existingUser -> {
                throw new EntityUniquePropertyException(Player.class, "name", existingUser.getName());
            });
        }
        return playerRepository.save(playerPatched);
    }

    public void deletePlayers() {
        playerRepository.deleteAll();
    }

    public void deletePlayer(Long id) {
        playerRepository.deleteById(getPlayer(id).getId()); // so, if playerId doesnt not exist throw exception
    }
}
