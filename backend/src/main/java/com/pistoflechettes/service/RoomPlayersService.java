package com.pistoflechettes.service;

import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.form.PlayerForm;
import com.pistoflechettes.model.RoomPlayers;
import com.pistoflechettes.model.Team;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.room.Room;
import com.pistoflechettes.repository.RoomPlayersRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomPlayersService {

    private final RoomPlayersRepository roomPlayersRepository;

    private final TeamService teamService;

    private final PlayerService playerService;

    public RoomPlayers getRoomPlayers(Long id) {
        Optional<RoomPlayers> roomPlayersOptional = roomPlayersRepository.findById(id);

        if (!roomPlayersOptional.isPresent()) {
            throw new EntityNotFoundException(RoomPlayers.class, "id", id.toString());
        }
        return roomPlayersOptional.get();
    }

    public RoomPlayers addRoomPlayers(RoomPlayers roomPlayers) {
        return roomPlayersRepository.save(roomPlayers);
    }

    public void addRoomsPlayers(List<PlayerForm> roomPlayersList, Room room) {

        List<RoomPlayers> roomPlayers = new ArrayList<>();

        for (PlayerForm rp : roomPlayersList) {
            Player player = playerService.getPlayer(rp.getId());
            Team team = teamService.getTeam(rp.getTeam_id());
            roomPlayers.add(addRoomPlayers(new RoomPlayers(player, team, room, rp.getPlayerOrder())));
        }
    }

    public List<RoomPlayers> all() {
        return roomPlayersRepository.findAll();
    }

    public void deleteRoomPlayers(Long id) {
        roomPlayersRepository.findById(getRoomPlayers(id).getId());
    }

    public List<RoomPlayers> findAllByRoom(Room room, Sort sort) {
        List<RoomPlayers> roomPlayers = roomPlayersRepository.findAllByRoom(room, sort);
        if (roomPlayers.isEmpty()) {
            throw new EntityNotFoundException(RoomPlayers.class, "roomId", room.getId().toString());
        }
        return roomPlayers;
    }
}
