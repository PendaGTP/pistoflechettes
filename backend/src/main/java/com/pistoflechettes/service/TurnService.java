package com.pistoflechettes.service;

import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.form.TurnForm;
import com.pistoflechettes.model.Turn;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.room.Room;
import com.pistoflechettes.repository.TurnRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TurnService {

    private final TurnRepository turnRepository;

    public Turn addTurn(Turn turn) {
        return turnRepository.save(turn);
    }

    public Turn addTurn(Room room, TurnForm turnForm, Player player) {
        Turn turn = new Turn();
        turn.setRoom(room);
        turn.setRoundNumber(turnForm.getRoundNumber());
        turn.setPlayer(player);

        return turnRepository.save(turn);
    }

    public List<Turn> all(Specification<Turn> spec, Sort sort) {
        List<Turn> turns = turnRepository.findAll(spec, sort);
        if (turns.isEmpty()) {
            throw new EntityNotFoundException(Turn.class);
        }
        return turns;
    }

    public Turn getTurn(Long id) {
        Optional<Turn> turnOptional = turnRepository.findById(id);
        if (!turnOptional.isPresent()) {
            throw new EntityNotFoundException(Turn.class, "id", id.toString());
        }
        return turnOptional.get();
    }

    public void deleteTurn(Long id) {
        turnRepository.deleteById(getTurn(id).getId());
    }

    public List<Turn> findAllByRoomId(Long roomId, Sort sort) {
        List<Turn> turns = turnRepository.findAllByRoomId(roomId, sort);

        if (turns.isEmpty()) {
            throw new EntityNotFoundException(Turn.class, "roomId", roomId.toString());
        }
        return turns;
    }
}
