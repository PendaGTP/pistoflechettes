package com.pistoflechettes.service;

import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.model.RoomMode;
import com.pistoflechettes.repository.RoomModeRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomModeService {

    private final RoomModeRepository roomModeRepository;

    public RoomMode addRoomMode(RoomMode roomMode) {
        Optional<RoomMode> roomModeOptional = roomModeRepository.findByMode(roomMode.getMode());
        return roomModeOptional.orElseGet(() -> roomModeRepository.save(roomMode));
    }

    public List<RoomMode> all(Specification<RoomMode> spec, Sort sort) {
        List<RoomMode> roomModes = roomModeRepository.findAll(spec, sort);
        if (roomModes.isEmpty()) {
            throw new EntityNotFoundException(RoomMode.class);
        }
        return roomModes;
    }

    public RoomMode getRoomMode(Long id) {
        Optional<RoomMode> roomModeOptional = roomModeRepository.findById(id);
        if (!roomModeOptional.isPresent()) {
            throw new EntityNotFoundException(RoomMode.class, "id", id.toString());
        }
        return roomModeOptional.get();
    }

    public void deleteRoomModes() {
        roomModeRepository.deleteAll();
    }

    public void deleteRoomMode(Long id) {
        roomModeRepository.deleteById(getRoomMode(id).getId());
    }

}
