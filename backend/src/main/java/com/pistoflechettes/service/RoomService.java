package com.pistoflechettes.service;

import com.pistoflechettes.enums.Status;
import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.model.room.Room;
import com.pistoflechettes.model.room.RoomCreationDTO;
import com.pistoflechettes.repository.RoomRepository;
import com.pistoflechettes.utils.ApiUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.json.JsonMergePatch;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoomService {

    private final ApiUtils apiUtils;
    private final RoomRepository roomRepository;
    private final RoomStatusService roomStatusService;
    private final RoomModeService roomModeService;
    private final RoomConfigService roomConfigService;

    public Room addRoom(RoomCreationDTO roomCreationDTO) {
        Room room = new Room();
        room.setName(roomCreationDTO.getName());
        room.setRoomMode(roomModeService.getRoomMode(roomCreationDTO.getRoom_mode_id()));
        room.setRoomConfig(roomConfigService.addRoomConfig(roomCreationDTO.getRoom_config()));
        startRoom(room);

        return roomRepository.save(room);
    }

    public List<Room> all(Specification<Room> spec, Sort sortBy) {
        List<Room> rooms = roomRepository.findAll(spec, sortBy);
        if (rooms.isEmpty()) {
            throw new EntityNotFoundException(Room.class);
        }
        return rooms;
    }

    public Room getRoom(Long id) {
        Optional<Room> roomOptional = roomRepository.findById(id);
        if (!roomOptional.isPresent()) {
            throw new EntityNotFoundException(Room.class, "id", id.toString());
        }
        return roomOptional.get();
    }

    public Room updateRoom(Long id, JsonMergePatch mergePatchDocument) {
        Room room = getRoom(id);
        Room roomUpdated = apiUtils.mergePatch(mergePatchDocument, room, Room.class);
        return roomRepository.save(roomUpdated);
    }

    public void deleteRooms() {
        roomRepository.deleteAll();
    }

    public void deleteRoom(Long id) {
        roomRepository.deleteById(id);
    }


    /* private methods */
    private Room getRoomByStatus(RoomStatus roomStatus) {
        Optional<Room> room = roomRepository.findByRoomStatus(roomStatus);
        return room.orElse(null);
    }

    private void setRunningRoomToPending(RoomStatus running, RoomStatus pending) {
        Room runningRoom = getRoomByStatus(running);
        if (runningRoom != null) {
            runningRoom.setRoomStatus(pending);
        }
    }

    private void startRoom(Room room) {
        // Receive RoomStatus
        RoomStatus running = roomStatusService.getRoomStatusByStatus(Status.Running);
        RoomStatus pending = roomStatusService.getRoomStatusByStatus(Status.Pending);

        // If a game is running set it to pending
        setRunningRoomToPending(running, pending);

        // Set the new game to running
        room.setRoomStatus(running);
    }
}
