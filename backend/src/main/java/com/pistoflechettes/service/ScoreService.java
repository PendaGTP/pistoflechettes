package com.pistoflechettes.service;


import com.pistoflechettes.model.Turn;
import com.pistoflechettes.model.score.Dart;
import com.pistoflechettes.model.score.Position;
import com.pistoflechettes.model.score.PositionDTO;
import com.pistoflechettes.model.score.ScoreCreationDTO;
import com.pistoflechettes.repository.DartRepository;
import com.pistoflechettes.repository.PositionRepository;
import com.pistoflechettes.responses.ScoreResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ScoreService {

    private final PositionRepository positionRepository;

    private final DartRepository dartRepository;

    public ScoreResponse addScore(ScoreCreationDTO scoreForm, Long room_id, Turn turn, Long player_id) {

        PositionDTO positionForm = scoreForm.getPosition();

        // Check if position already exist
        Optional<Position> optionalPosition = positionRepository.findByXAndAndYAndDepth(positionForm.getX(), positionForm.getY(), positionForm.getDepth());
        if (!optionalPosition.isPresent()) { // not exist
            Position position = positionRepository.save(new Position(positionForm.getX(), positionForm.getY(), positionForm.getDepth()));
            Dart dart = dartRepository.save(new Dart(scoreForm.getScore(), position, turn, scoreForm.getLabel(), scoreForm.getDartNumber()));

            return new ScoreResponse(room_id, turn.getId(), player_id, dart);
        }

        Position position = optionalPosition.get();
        Dart dart = dartRepository.save(new Dart(scoreForm.getScore(), position, turn, scoreForm.getLabel(), scoreForm.getDartNumber()));

        return new ScoreResponse(room_id, turn.getId(), player_id, dart);
    }
}
