package com.pistoflechettes.service;

import com.pistoflechettes.exception.EntityNotFoundException;
import com.pistoflechettes.model.Team;
import com.pistoflechettes.repository.TeamRepository;
import com.pistoflechettes.utils.ApiUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.json.JsonMergePatch;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TeamService {

    private final ApiUtils apiUtils;
    private final TeamRepository teamRepository;

    public Team addTeam(Team team) {
        return teamRepository.save(team);
    }

    public List<Team> all(Specification<Team> spec, Sort sort) {
        List<Team> teams = teamRepository.findAll(spec, sort);
        if (teams.isEmpty()) {
            throw new EntityNotFoundException(Team.class);
        }
        return teams;
    }

    public Team getTeam(Long id) {
        Optional<Team> teamOptional = teamRepository.findById(id);
        if (!teamOptional.isPresent()) {
            throw new EntityNotFoundException(Team.class, "id", id.toString());
        }
        return teamOptional.get();
    }

    public Team updateTeam(Long id, JsonMergePatch mergePatchDocument) {
        Team team = getTeam(id);
        Team teamPatched = apiUtils.mergePatch(mergePatchDocument, team, Team.class);
        return teamRepository.save(teamPatched);
    }

    public void deleteTeams() {
        teamRepository.deleteAll();
    }

    public void deleteTeam(Long id) {
        teamRepository.deleteById(getTeam(id).getId());
    }
}
