package com.pistoflechettes.utils;

import javax.json.JsonMergePatch;
import javax.json.JsonPatch;

public interface IApiUtils {
    <T> T patch(JsonPatch patch, T targetBean, Class<T> beanClass);

    <T> T mergePatch(JsonMergePatch mergePatch, T targetBean, Class<T> beanClass);
}
