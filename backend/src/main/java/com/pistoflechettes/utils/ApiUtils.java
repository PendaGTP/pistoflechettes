package com.pistoflechettes.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.json.JsonMergePatch;
import javax.json.JsonPatch;
import javax.json.JsonStructure;
import javax.json.JsonValue;

@Service
@AllArgsConstructor
public class ApiUtils implements IApiUtils {

    private final ObjectMapper mapper;

    @Override
    public <T> T patch(JsonPatch patch, T targetBean, Class<T> beanClass) {
        // Convert the Java bean to a JSON document
        JsonStructure target = mapper.convertValue(targetBean, JsonStructure.class);

        // Apply the JSON Patch to the JSON document
        JsonValue patched = patch.apply(target);

        // Convert the JSON document to a Java bean and return it
        return mapper.convertValue(patched, beanClass);
    }

    @Override
    public <T> T mergePatch(JsonMergePatch mergePatch, T targetBean, Class<T> beanClass) {
        // Convert the Java bean to a JSON document
        JsonValue target = mapper.convertValue(targetBean, JsonValue.class);

        // Apply the JSON Merge Patch to the JSON document
        JsonValue patched = mergePatch.apply(target);

        // Convert the JSON document to a Java bean and return it
        return mapper.convertValue(patched, beanClass);
    }
}
