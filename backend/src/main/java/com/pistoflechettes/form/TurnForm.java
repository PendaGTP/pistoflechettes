package com.pistoflechettes.form;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TurnForm {

    @JsonProperty("round_number")
    private Long roundNumber;
    @JsonProperty("player_id")
    private Long playerId;

    public TurnForm(Long roundNumber, Long playerId) {
        this.roundNumber = roundNumber;
        this.playerId = playerId;
    }


    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(Long roundNumber) {
        this.roundNumber = roundNumber;
    }
}
