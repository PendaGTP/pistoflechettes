package com.pistoflechettes.form;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerForm {

    private Long id;
    private Long team_id;
    @JsonProperty("player_order")
    private Long playerOrder;

    PlayerForm() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Long team_id) {
        this.team_id = team_id;
    }


    public Long getPlayerOrder() {
        return playerOrder;
    }

    public void setPlayerOrder(Long playerOrder) {
        this.playerOrder = playerOrder;
    }
}
