package com.pistoflechettes.responses;

import com.pistoflechettes.model.score.Dart;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class ScoreResponse {

    private Long room_id;
    private Long turn_id;
    private Long player_id;
    private Dart dart;


    public ScoreResponse(Long room_id, Long turn_id, Long player_id, Dart dart) {
        this.room_id = room_id;
        this.turn_id = turn_id;
        this.player_id = player_id;
        this.dart = dart;
    }
}
