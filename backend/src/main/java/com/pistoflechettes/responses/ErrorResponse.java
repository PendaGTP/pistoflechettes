package com.pistoflechettes.responses;

import java.time.LocalDateTime;

public class ErrorResponse {


    private String error_message;

    public ErrorResponse() {
    }

    public ErrorResponse(String error) {
        LocalDateTime timestamp = LocalDateTime.now();
        error_message = error;
    }

    public ErrorResponse setEntityNotFound(String nomClass, Long id) {
        error_message = nomClass + " was not found for id=" + id;
        return this;
    }

    public String getError_message() {
        return error_message;
    }

    public ErrorResponse setError_message(String error_message) {
        this.error_message = error_message;
        return this;
    }
}
