package com.pistoflechettes.config;

import com.pistoflechettes.enums.Mode;
import com.pistoflechettes.enums.Status;
import com.pistoflechettes.model.RoomMode;
import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.repository.RoomModeRepository;
import com.pistoflechettes.repository.RoomStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DataLoader implements ApplicationRunner {

    private final RoomModeRepository roomModeRepository;
    private final RoomStatusRepository roomStatusRepository;

    @Override
    public void run(ApplicationArguments args) {
        createRoomMode();
        createRoomStatus();
    }

    private void createRoomMode() {
        roomModeRepository.save(new RoomMode(Mode.Normal));
        roomModeRepository.save(new RoomMode(Mode.Boat));
    }

    private void createRoomStatus() {
        roomStatusRepository.save(new RoomStatus(Status.Running));
        roomStatusRepository.save(new RoomStatus(Status.Pending));
        roomStatusRepository.save(new RoomStatus(Status.Finished));
    }
}
