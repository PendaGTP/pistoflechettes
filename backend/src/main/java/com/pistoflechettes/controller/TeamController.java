package com.pistoflechettes.controller;

import com.pistoflechettes.model.Team;
import com.pistoflechettes.service.TeamService;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonMergePatch;
import java.util.List;

@RestController
@RequestMapping("/teams")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Team> addTeam(@RequestBody Team newTeam) {
        Team team = teamService.addTeam(newTeam);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<Team>> getAll(
            @Spec(path = "name", spec = Like.class) Specification<Team> spec,
            Sort sort) {

        List<Team> teams = teamService.all(spec, sort);
        return new ResponseEntity<>(teams, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Team> getTeam(@PathVariable Long id) {
        Team team = teamService.getTeam(id);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    @PatchMapping(
            value = "/{id}",
            consumes = "application/merge-patch+json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Team> updateTeam(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {
        Team team = teamService.updateTeam(id, mergePatchDocument);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Void> deleteTeams() {
        teamService.deleteTeams();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteTeam(@PathVariable Long id) {
        teamService.deleteTeam(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
