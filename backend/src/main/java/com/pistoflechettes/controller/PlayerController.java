package com.pistoflechettes.controller;

import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.player.PlayerDTO;
import com.pistoflechettes.model.player.PlayerMapper;
import com.pistoflechettes.service.PlayerService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonMergePatch;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/players")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class PlayerController {

    private final PlayerService playerService;

    @Operation(summary = "Create a new player")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PlayerDTO> addPlayer(@Valid @RequestBody PlayerDTO playerDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || (playerDTO == null)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Player player = playerService.addPlayer(PlayerMapper.INSTANCE.toPlayer(playerDTO));
        return new ResponseEntity<>(PlayerMapper.INSTANCE.toPlayerDTO(player), HttpStatus.OK);
    }

    @Operation(summary = "Get all players")
    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<PlayerDTO>> getAll(
            @And({
                    @Spec(path = "id", paramSeparator = ',', spec = In.class),
                    @Spec(path = "name", spec = Like.class),
                    @Spec(path = "regular", spec = Equal.class)
            }) Specification<Player> spec,
            Sort sort) {
        List<Player> players = playerService.all(spec, sort.isUnsorted() ? Sort.by("id") : sort);
        return new ResponseEntity<>(PlayerMapper.INSTANCE.toPlayerDTOs(players), HttpStatus.OK);
    }

    @Operation(summary = "Get player by id")
    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PlayerDTO> getPlayer(@PathVariable Long id) {
        Player player = playerService.getPlayer(id);
        return new ResponseEntity<>(PlayerMapper.INSTANCE.toPlayerDTO(player), HttpStatus.OK);
    }

    @Operation(summary = "Modify player attribute(s)")
    @PatchMapping(
            value = "/{id}",
            consumes = "application/merge-patch+json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Player> updatePlayer(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {
        Player player = playerService.updatePlayer(id, mergePatchDocument);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @Operation(summary = "Delete all player")
    @DeleteMapping()
    public ResponseEntity<Void> deletePlayers() {
        playerService.deletePlayers();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Delete the player")
    @DeleteMapping(
            value = "/{id}"
    )
    public ResponseEntity<Void> deletePlayer(@PathVariable Long id) {
        playerService.deletePlayer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
