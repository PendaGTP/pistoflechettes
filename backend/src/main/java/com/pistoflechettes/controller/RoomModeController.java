package com.pistoflechettes.controller;

import com.pistoflechettes.model.RoomMode;
import com.pistoflechettes.service.RoomModeService;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/roommodes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class RoomModeController {

    private final RoomModeService roomModeService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomMode> addRoomMode(@RequestBody RoomMode newRoomMode) {
        RoomMode roomMode = roomModeService.addRoomMode(newRoomMode);
        return new ResponseEntity<>(roomMode, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<RoomMode>> getAll(
            @Spec(path = "mode", spec = Like.class) Specification<RoomMode> spec,
            Sort sort) {
        List<RoomMode> roomModes = roomModeService.all(spec, sort);
        return new ResponseEntity<>(roomModes, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomMode> getRoomMode(@PathVariable Long id) {
        RoomMode roomMode = roomModeService.getRoomMode(id);
        return new ResponseEntity<>(roomMode, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Void> deleteRoomModes() {
        roomModeService.deleteRoomModes();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @DeleteMapping(
            value = "/{id}"
    )
    public ResponseEntity<Void> deleteRoomMode(@PathVariable Long id) {
        roomModeService.deleteRoomMode(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
