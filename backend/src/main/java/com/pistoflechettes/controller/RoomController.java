package com.pistoflechettes.controller;

import com.pistoflechettes.form.TurnForm;
import com.pistoflechettes.model.RoomPlayers;
import com.pistoflechettes.model.Turn;
import com.pistoflechettes.model.player.Player;
import com.pistoflechettes.model.room.Room;
import com.pistoflechettes.model.room.RoomCreationDTO;
import com.pistoflechettes.model.room.RoomDTO;
import com.pistoflechettes.model.room.RoomMapper;
import com.pistoflechettes.model.score.ScoreCreationDTO;
import com.pistoflechettes.responses.ScoreResponse;
import com.pistoflechettes.service.*;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonMergePatch;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/rooms")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class RoomController {

    private final RoomService roomService;

    private final ScoreService scoreService;

    private final RoomPlayersService roomPlayersService;

    private final TurnService turnService;

    private final PlayerService playerService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Room> addRoom(@Valid @RequestBody RoomCreationDTO roomCreationDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || (roomCreationDTO == null)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Room room = roomService.addRoom(roomCreationDTO);
        roomPlayersService.addRoomsPlayers(roomCreationDTO.getPlayers(), room);
        return new ResponseEntity<>(room, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<RoomDTO>> getAll(
            @Join(path = "roomStatus", alias = "rs")
            @Join(path = "roomMode", alias = "rm")
            @Join(path = "winnerId", alias = "p")
            @And({
                    @Spec(path = "id", paramSeparator = ',', spec = In.class),
                    @Spec(path = "name", spec = Like.class),
                    @Spec(path = "rs.status", params = "status", spec = Equal.class),
                    @Spec(path = "createdAt", params = "created_at", spec = LessThanOrEqual.class),
                    @Spec(path = "createdAt", params = "created_at", spec = GreaterThanOrEqual.class),
                    @Spec(path = "endAt", params = "end_at", spec = LessThanOrEqual.class),
                    @Spec(path = "endAt", params = "end_at", spec = GreaterThanOrEqual.class),
                    @Spec(path = "rm.mode", params = "mode", spec = Equal.class),
                    @Spec(path = "p.name", params = "winnerName", spec = Like.class),
                    @Spec(path = "p.id", params = "winnerId", spec = Equal.class)
            }) Specification<Room> spec,
            Sort sort) {
        List<Room> rooms = roomService.all(spec, sort.isUnsorted() ? Sort.by("id") : sort);
        return new ResponseEntity<>(RoomMapper.INSTANCE.toRoomDTOs(rooms), HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomDTO> getRoom(@PathVariable Long id) {
        Room room = roomService.getRoom(id);
        return new ResponseEntity<>(RoomMapper.INSTANCE.toRoomDTO(room), HttpStatus.OK);
    }

    @PatchMapping(
            value = "/{id}",
            consumes = "application/merge-patch+json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomDTO> updateRoom(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {
        System.out.println("Update room");
        Room room = roomService.updateRoom(id, mergePatchDocument);
        return new ResponseEntity<>(RoomMapper.INSTANCE.toRoomDTO(room), HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Void> deleteRooms() {
        roomService.deleteRooms();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(
            value = "/{id}"
    )
    public ResponseEntity<Void> deleteRoom(@PathVariable Long id) {
        roomService.deleteRoom(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    /* -------- Get players in room -------- */
    @GetMapping(
            value = "/{id}/players",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<RoomPlayers>> getPlayersInRoomSortBy(@PathVariable Long id, Sort sort) {
        Room room = roomService.getRoom(id);
        List<RoomPlayers> roomPlayers = roomPlayersService.findAllByRoom(room, sort);
        return new ResponseEntity<>(roomPlayers, HttpStatus.OK);
    }

    /* -------- Add Turn to Room -------- */
    @PostMapping(
            value = "/{roomId}/turns",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Turn> addTurnToRoom(@PathVariable Long roomId, @Valid @RequestBody TurnForm turnForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || (turnForm == null)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Room room = roomService.getRoom(roomId);
        Player player = playerService.getPlayer(turnForm.getPlayerId());
        Turn turn = turnService.addTurn(room, turnForm, player);
        return new ResponseEntity<>(turn, HttpStatus.OK);
    }

    /* -------- Add Position/Dart to a Rooms/Turns/Player -------- */
    @PostMapping(
            value = "/{roomId}/turns/{turnId}/players/{playerId}/darts",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ScoreResponse> addScoreToPlayerToTurnToRoom(@PathVariable Long roomId, @PathVariable Long turnId, @PathVariable Long playerId, @Valid @RequestBody ScoreCreationDTO scoreForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || (scoreForm == null)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Room room = roomService.getRoom(roomId);
        Turn turn = turnService.getTurn(turnId);
        Player player = playerService.getPlayer(playerId);

        ScoreResponse scoreResponse = scoreService.addScore(scoreForm, room.getId(), turn, player.getId());

        return new ResponseEntity<>(scoreResponse, HttpStatus.OK);
    }

    /* -------- Get all turns in a room -------- */
    @GetMapping(
            value = "/{id}/turns",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<Turn>> getTurnsInRoom(
            @PathVariable Long id,
            @Join(path = "player", alias = "p")
            @And({
                    @Spec(path = "turnNumber", spec = Equal.class),
                    @Spec(path = "p.name", spec = Like.class),
                    @Spec(path = "p.id", spec = Like.class)
            }) Specification<Turn> spec,
            Sort sort
    ) {
        List<Turn> turns = turnService.findAllByRoomId(id, sort);
        return new ResponseEntity<>(turns, HttpStatus.OK);
    }
}

