package com.pistoflechettes.controller;

import com.pistoflechettes.model.RoomStatus;
import com.pistoflechettes.service.RoomStatusService;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonMergePatch;
import java.util.List;

@RestController
@RequestMapping(value = "/roomstatus")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class RoomStatusController {

    private final RoomStatusService roomStatusService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomStatus> addRoomStatus(@RequestBody RoomStatus newRoomStatus) {
        RoomStatus roomStatus = roomStatusService.addRoomStatus(newRoomStatus);
        return new ResponseEntity<>(roomStatus, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<RoomStatus>> getAll(
            @Spec(path = "status", spec = Like.class) Specification<RoomStatus> spec,
            Sort sort) {
        List<RoomStatus> roomStatusList = roomStatusService.all(spec, sort);
        return new ResponseEntity<>(roomStatusList, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomStatus> getRoomStatus(@PathVariable Long id) {
        RoomStatus roomStatus = roomStatusService.getRoomStatus(id);
        return new ResponseEntity<>(roomStatus, HttpStatus.OK);
    }

    @PatchMapping(
            value = "/{id}",
            consumes = "application/merge-patch+json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<RoomStatus> updateRoomStatus(@PathVariable Long id, @RequestBody JsonMergePatch mergePatchDocument) {
        RoomStatus roomStatus = roomStatusService.updateRoomStatus(id, mergePatchDocument);
        return new ResponseEntity<>(roomStatus, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Void> deleteAllRoomStatus() {
        roomStatusService.deleteAllRoomStatus();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteRoomStatus(@PathVariable Long id) {
        roomStatusService.deleteRoomStatus(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
