package com.pistoflechettes.controller;

import com.pistoflechettes.model.Turn;
import com.pistoflechettes.service.TurnService;
import lombok.AllArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/turns")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@AllArgsConstructor
public class TurnController {

    private final TurnService turnService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Turn> addTurn(@RequestBody Turn newTurn) {
        Turn turn = turnService.addTurn(newTurn);
        return new ResponseEntity<>(turn, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<Turn>> getAll(
            @Spec(path = "turnNumber", spec = Equal.class) Specification<Turn> spec,
            Sort sort) {
        List<Turn> turns = turnService.all(spec, sort);
        return new ResponseEntity<>(turns, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Turn> getTurn(@PathVariable Long id) {
        Turn turn = turnService.getTurn(id);
        return new ResponseEntity<>(turn, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteTurn(@PathVariable Long id) {
        turnService.deleteTurn(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
