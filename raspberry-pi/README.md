# Run this project with an Raspberry PI 3

## Prerequisites
* Install Raspbian Lite on a Raspberry PI 3

## Inital Raspbian setup
* Login with default login : ```pi / raspberry```
* Run ```sudo raspi-config``` to configure keyboard and timezone
* Run ```sudo systemctl enable ssh.service``` to start ssh at startup
* Connect your raspberry to your network :
    * Wifi : edit ```/etc/wpa_supplicant/wpa_supplicant.conf``` :
```bash
....
country=fr
update_config=1

network={
 scan_ssid=1
 ssid="SSID_NAME"
 psk="WPA_PASSWORD"
}
```
* Add ip reservation in your network provider
* Run ```sudo apt update``` to update system's package
* Run ```sudo apt full-upgrade``` to upgrade all installed packages to their latest versions
* Run ```sudo apt clean```to clean .deb, get some free space